import React, { useState } from "react";
import { GET_COMPANIES, ADD_COMPANY } from "./Page.queries";
import { useMutation, useQuery } from "@apollo/client";
import { SectorsCard } from "components/SectorsCard/SectorsCard";
import { InvestmentSizeCard } from "components/InvestmentSizeCard/InvestmentSizeCard";
import { CompaniesTable } from "components/ComapniesTable/CompaniesTable";
import { Modal } from "components/Modal/Modal";
import { Form } from "components/Form/Form";

function generateRandomColor() {
  const randomHex = Math.floor(Math.random() * 16777215).toString(16);
  return `#${randomHex}`;
}

export const Page = () => {
  const { loading, error, data: companyData } = useQuery(GET_COMPANIES);
  const [addCompany] = useMutation(ADD_COMPANY, {
    refetchQueries: [{ query: GET_COMPANIES }],
  });
  const [isModalVisible, setModalVisbility] = useState(false);
  const [formError, setFormError] = useState("");

  if (loading) {
    return <span>Loading data...</span>;
  }

  if (error) {
    return (
      <span>
        <pre>{JSON.stringify(error, null, 2)}</pre>
      </span>
    );
  }

  const { companies } = companyData;

  const emptySectors = {
    insuretech: 0,
    iot: 0,
    roboadvisory: 0,
    fintech: 0,
  };
  const sectorsCount = companies.reduce((result, company) => {
    return {
      ...result,
      [company.sector.toLowerCase()]: result[company.sector.toLowerCase()] + 1,
    };
  }, emptySectors);

  const chartData = companies.map((company) => ({
    title: company.name,
    color: generateRandomColor(),
    value: company.investmentSize,
  }));

  const handleToggleAddModal = (event) => {
    event.preventDefault();
    event.stopPropagation();
    setFormError("");
    setModalVisbility(!isModalVisible);
  };

  const handleAddCompany = async (values) => {
    const { name, investmentSize, sector, stage } = values;
    try {
      await addCompany({
        variables: {
          name,
          sector,
          stage,
          investmentSize: Number(investmentSize),
        },
      });
      setModalVisbility(false);
    } catch (error) {
      setFormError(error.message || "error has occured");
    }
  };

  return (
    <>
      <SectorsCard sectorsCount={sectorsCount} />
      <InvestmentSizeCard chartData={chartData} />
      <CompaniesTable
        companies={companies}
        handleOpenAddModal={handleToggleAddModal}
      />
      <Modal
        title="Add new company"
        isVisible={isModalVisible}
        onClose={handleToggleAddModal}
      >
        <Form onSubmit={handleAddCompany} error={formError} />
      </Modal>
    </>
  );
};

export default Page;
