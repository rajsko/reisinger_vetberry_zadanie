import React from 'react'
import PropTypes from 'prop-types'
import styles from './Card.scss'

export function Card (props) {
  return (
    <div className={styles.card}>
      <div className={styles.header}>
        <h3 className={styles.title}>{props.title}</h3>
      </div>
      <div className={styles.content}>{props.children}</div>
    </div>
  )
}

Card.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
}
