import React from 'react'
import PropTypes from 'prop-types'

import styles from 'components/Modal/Modal.scss'

export function Modal (props) {
  if (!props.isVisible) {
    return null
  }
  return (
    <div className={styles.backdrop} onClick={props.onClose}>
      <div
        className={styles.modal}
        onClick={(event) => event.stopPropagation()}
      >
        <div className={styles.title}>
          <h3>{props.title}</h3>
        </div>
        <div className={styles.content}>{props.children}</div>
      </div>
    </div>
  )
}

Modal.propTypes = {
  isVisible: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  title: PropTypes.string,
  children: PropTypes.node,
}
