import React from 'react'
import Select from 'react-select'
import {useFormik} from 'formik'
import PropTypes from 'prop-types'

import styles from 'components/Form/Form.scss'
import {SecondaryButton} from 'components/Buttons/Buttons'

const SECTORS = [
  'Fintech',
  'IOT',
  'Roboadvisory',
  'Insuretech',
].map((sector) => ({value: sector, label: sector}))
const STAGES = [
  'Idea',
  'Prototype',
  'Seed',
  'Series A',
  'Series B',
  'Series C',
].map((stage) => ({value: stage, label: stage}))

export function Form ({onSubmit, error}) {
  const formik = useFormik({
    initialValues: {
      name: '',
      sector: '',
      stage: '',
      investmentSize: '',
    },
    onSubmit,
  })
  return (
    <form onSubmit={formik.handleSubmit}>
      <label className={styles.label} htmlFor="name">
        Company name
      </label>
      <input
        className={styles.input}
        id="name"
        name="name"
        type="text"
        placeholder="Company name"
        onChange={formik.handleChange}
        value={formik.values.name}
      />
      <label className={styles.label} htmlFor="stage">
        Stage
      </label>
      <Select
        className={styles.select}
        name="stage"
        id="stage"
        placeholder="Stage"
        options={STAGES}
        onChange={(option) => {
          formik.setFieldValue('stage', option.value)
        }}
        setFieldValue={formik.values.stage}
      />
      <label className={styles.label} htmlFor="sector">
        Sector
      </label>
      <Select
        className={styles.select}
        name="sector"
        id="sector"
        placeholder="Sector"
        options={SECTORS}
        onChange={(option) => {
          formik.setFieldValue('sector', option.value)
        }}
        setFieldValue={formik.values.sector}
      />
      <label className={styles.label} htmlFor="investmentSize">
        Investment size
      </label>
      <div className={styles.wrap}>
        <input
          className={styles.input}
          id="investmentSize"
          name="investmentSize"
          type="tel"
          placeholder="Investment size"
          onChange={(event) => {
            const value = event.target.value
            const validated = value.replace(/[^0-9]/g, '')
            formik.setFieldValue('investmentSize', validated)
          }}
          value={formik.values.investmentSize}
        />
        <span className={styles.currency}>EUR</span>
      </div>
      <p className={styles.error}>{error}</p>
      <SecondaryButton className={styles.button} type="submit">
        Add new company
      </SecondaryButton>
    </form>
  )
}

Form.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  error: PropTypes.string,
}
