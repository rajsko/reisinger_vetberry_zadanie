import React from 'react'
import PropTypes from 'prop-types'
import styles from 'components/ComapniesTable/CompaniesTable.scss'
import sharedStyles from 'components/Card/Card.scss'
import {PrimaryButton} from 'components/Buttons/Buttons'

export function CompaniesTable ({companies, handleOpenAddModal}) {
  return (
    <div className={`${sharedStyles.card} ${styles.wrapper}`}>
      <table className={styles.table}>
        <thead>
          <tr className={`${styles.row} ${sharedStyles.header}`}>
            <th
              className={`${styles.col} ${styles.title} ${styles.name}  ${sharedStyles.title}`}
            >
              COMPANY NAME
            </th>
            <th
              className={`${styles.col} ${styles.title} ${styles.stage}  ${sharedStyles.title}`}
            >
              STAGE
            </th>
            <th
              className={`${styles.col} ${styles.title} ${styles.sector}  ${sharedStyles.title}`}
            >
              SECTOR
            </th>
            <th
              className={`${styles.col} ${styles.title} ${styles.investmentSize}  ${sharedStyles.title}`}
            >
              INVESTMENT SIZE
            </th>
          </tr>
        </thead>
        <tbody className={sharedStyles.content}>
          {companies.map((company, i) => (
            <tr key={i} className={`${styles.dataRow}`}>
              <td className={`${styles.col}  ${styles.name}`}>
                {company.name}
              </td>
              <td className={`${styles.col} ${styles.stage}`}>
                {company.stage}
              </td>
              <td className={`${styles.col} ${styles.sector}`}>
                {company.sector}
              </td>
              <td className={`${styles.col} ${styles.investmentSize}`}>
                {new Intl.NumberFormat('sk', {
                  style: 'decimal',
                  currency: 'EUR',
                }).format(company.investmentSize)}{' '}
                EUR
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      <div className={styles.footer}>
        <PrimaryButton
          type="button"
          onClick={handleOpenAddModal}
          className={styles.button}
        >
          Add new company
        </PrimaryButton>
      </div>
    </div>
  )
}

CompaniesTable.propTypes = {
  companies: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRquired,
      stage: PropTypes.string.isRquired,
      sector: PropTypes.string.isRquired,
      investmentSize: PropTypes.number.isRquired,
    })
  ),
  handleOpenAddModal: PropTypes.func,
}
