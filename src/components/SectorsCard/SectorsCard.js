import React from 'react'
import PropTypes from 'prop-types'

import {Card} from 'components/Card/Card'
import styles from 'components/SectorsCard/SectorsCard.scss'

export function SectorsCard (props) {
  const {iot, fintech, insuretech, roboadvisory} = props.sectorsCount
  return (
    <Card title="Companies by sectors">
      <div className={styles.sectors}>
        <div className={styles.sector}>
          <p className={styles.count}>{fintech}</p>
          <p className={styles.caption}>fintech</p>
          <img
            className={styles.icon}
            src="../src/assets/icons/ico_fintech.svg"
          />
        </div>
        <div className={styles.sector}>
          <p className={styles.count}>{insuretech}</p>
          <p className={styles.caption}>insuretech</p>
          <img
            className={styles.icon}
            src="../src/assets/icons/ico_insurtech.svg"
          />
        </div>
        <div className={styles.sector}>
          <p className={styles.count}>{roboadvisory}</p>
          <p className={styles.caption}>roboadvisory</p>
          <img
            className={styles.icon}
            src="../src/assets/icons/ico_roboadvisory.svg"
          />
        </div>
        <div className={styles.sector}>
          <p className={styles.count}>{iot}</p>
          <p className={styles.caption}>iot</p>
          <img className={styles.icon} src="../src/assets/icons/ico_iot.svg" />
        </div>
      </div>
    </Card>
  )
}

SectorsCard.propTypes = {
  sectorsCount: PropTypes.exact({
    iot: PropTypes.number,
    insuretech: PropTypes.number,
    roboadvisory: PropTypes.number,
    fintech: PropTypes.number,
  }),
}
