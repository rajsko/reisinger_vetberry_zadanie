import React from 'react'
import PropTypes from 'prop-types'
import styles from 'components/Buttons/Buttons.scss'

function GeneralButton (props) {
  return (
    <button
      className={`${styles.button} ${styles.primary} ${props.className}`}
      type={props.type}
      onClick={props.onClick}
    >
      {props.children}
    </button>
  )
};

export function PrimaryButton (properties) {
  return (
    <GeneralButton
      {...properties}
      className={`${styles.primary} ${properties.className}`}
    />
  )
}

export function SecondaryButton (properties) {
  return (
    <GeneralButton
      {...properties}
      className={`${styles.secondary} ${properties.className}`}
    />
  )
}

GeneralButton.propTypes = {
  className: PropTypes.string,
  type: PropTypes.oneOf(['button', 'submit', 'reset']).isRequired,
  children: PropTypes.node.isRequired,
  onClick: PropTypes.func.isRequired,
}
