import React from 'react'
import {PieChart} from 'react-minimal-pie-chart'
import PropTypes from 'prop-types'

import {Card} from 'components/Card/Card'
import styles from 'components/InvestmentSizeCard/InvestmentSizeCard.scss'

export function InvestmentSizeCard ({chartData}) {
  return (
    <Card title="Companies by investment size">
      <div className={styles.content}>
        <div className={styles.piechart}>
          <div className={styles.label}>
            <span className={styles.labelNumber}>{chartData.length}</span>{' '}
            <br /> <span className={styles.labelText}>companies</span>
          </div>
          <PieChart data={chartData} lineWidth={30} />
        </div>
        <ul className={styles.companiesList}>
          {chartData.map((company, index) => (
            <li key={`${company}_${index}`} className={styles.companyListItem}>
              <span
                className={styles.dot}
                style={{backgroundColor: company.color}}
              />
              {company.title}
            </li>
          ))}
        </ul>
      </div>
    </Card>
  )
}

InvestmentSizeCard.propTypes = {
  chartData: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string.isRequired,
      color: PropTypes.string.isRequired,
      value: PropTypes.number,
    }),
  ),
}
